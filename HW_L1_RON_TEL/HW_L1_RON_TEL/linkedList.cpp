#include <iostream>
#include "linkedList.h"

using namespace std;

//add node in top of the list
Node* addToList(Node* head , int val) 
{
	Node* newHead = new Node;
	newHead->next = new Node;
	newHead->val = val;
	newHead->next = head;
	return newHead;
}

//removing the head of the list , returning head->next.
Node* removeFromList(Node* head)
{
	Node* toRet = NULL;
	if (head->next != NULL) 
		toRet = head->next;
	else 
	{
		toRet = new Node;
		toRet->next = new Node;
	}
	delete (head);
	return toRet;
}

//recursiv func to delete the given list
void recursiveClean(Node* h , Node* placeHolder)
{
	if (h == NULL || h == placeHolder)
		return;
	recursiveClean(h->next , placeHolder);
	delete (h);
}