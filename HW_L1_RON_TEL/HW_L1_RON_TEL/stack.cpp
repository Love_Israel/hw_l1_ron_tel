#include <iostream>
#include "linkedList.h"
#include "stack.h"

using namespace std;

Node* placeHolder = new Node; //global var , placeholder

void push(stack* s, unsigned int element) 
{
	if (s->head->next == NULL)
	{
		s->head->val = element;
		s->head->next = new Node;
		s->head->next = placeHolder;
		return;
	}
	s->head = addToList(s->head , element);
}
int pop(stack* s)
{
	int toRet = s->head->val;
	s->head = removeFromList(s->head);
	return toRet;
}

void initStack(stack* s) 
{
	s->head = new Node;
	s->head->next = new Node;
	s->head->next = NULL;
}

void cleanStack(stack* s)
{
	recursiveClean(s->head , placeHolder);
	delete (s);
}