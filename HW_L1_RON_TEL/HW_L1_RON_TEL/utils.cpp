#include <iostream>
#include "stack.h"

using namespace std;

//reverse given array using stack
void reverse(int* nums, unsigned int size)
{
	stack* myStack = new stack;
	initStack(myStack);
	for (int i = 0 ; i < size ; i++) 
	{
		push(myStack, nums[i]);
	}
	for (int i = 0; i < size; i++)
	{
		nums[i] = pop(myStack);
	}
}

//asking the user to put 10 numbers and reverse them
int* reverse10()
{
	int* arr = new int[10];
	cout << "please enter 10 numbers : ";
	for (int i = 0 ; i < 10 ; i++) 
	{
		cin >> arr[i];
	}
	reverse(arr, 10);
	return (arr);
}